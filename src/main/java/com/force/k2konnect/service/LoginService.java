package com.force.k2konnect.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.force.k2konect.model.User;
import com.force.k2konect.model.UserInfo;
import com.force.k2konnect.connector.RestApiConnector;
import com.force.k2konnect.http.K2konnectEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class LoginService {

    @Autowired
    private RestApiConnector restApiConnector;

    /**
     * "username=tbalancin@k2partnering.com.dev7&client_secret=3696027746615538381&password=Teste1234&grant_type=password&client_id=3MVG9Oe7T3Ol0ea4t2O2mDN1fArM_xKAbcxHMIEz249W7J3lABO.Fm3mPt3WRfIEkdkzhrTbTyNmxTLSuCqdN"
     *
     *
     */

    @Async
    public UserInfo getLogin(User user){
        /**
        user.setUsername("tbalancin@k2partnering.com.dev7");
        user.setPassword("Teste1234");
         **/

        user.setClient_id("3MVG9Oe7T3Ol0ea4t2O2mDN1fArM_xKAbcxHMIEz249W7J3lABO.Fm3mPt3WRfIEkdkzhrTbTyNmxTLSuCqdN");
        user.setClient_secret("3696027746615538381");
        user.setGrant_type("password");

        HttpEntity<Object> object = restApiConnector.connectorPostHttp(user, K2konnectEndpoint.URL_LOGIN);
        ObjectMapper objectMapper = new ObjectMapper();
       //
        UserInfo userInfo = null;

        //userInfo = objectMapper.readValue(object.getBody().toString(), UserInfo.class);
        userInfo = objectMapper.convertValue(object.getBody(), UserInfo.class);

        return userInfo;
    }

}
