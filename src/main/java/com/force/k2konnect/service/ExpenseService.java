package com.force.k2konnect.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.force.k2konect.model.*;
import com.force.k2konnect.connector.RestApiConnector;
import com.force.k2konnect.http.K2konnectEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ExpenseService {

    @Autowired
    private RestApiConnector restApiConnector;


    @Async
    public String sendExpense(RequestPhoto requestPhoto){


        ExpenseRequest expenseRequest = new ExpenseRequest();
        Payload payload = new Payload();
        EcAttachment ecAttachment = new EcAttachment();

        expenseRequest.setAction("saveExpense");

        //payload.setId("");
        payload.setStatus("Uploaded");
        payload.setPeriodId("a2QV00000038EjSMAU");
        payload.setDealsheetId("a0VV0000001SOTF");
        payload.setBillingPeriodId("a2QV00000038EjSMAU");


        ExpenseCards expenseCards = new ExpenseCards();
        expenseCards.setEcValue(100);
        expenseCards.setEcClient("Balancin");
        expenseCards.setEcComments("Coments");
        expenseCards.setEcCurrency("BRL");
        expenseCards.setEcDate("2017-03-30");
        expenseCards.setEcDescription("Description");
        expenseCards.setEcExchangeRate(1);
        expenseCards.setEcTransactionValue(100);
        expenseCards.setEcType("Travel");




        ecAttachment.setFileName("ImageTest.png");
        ecAttachment.setContentType("image/png");
        ecAttachment.setBase64Data(requestPhoto.getBytes());

        expenseCards.setEcAttachment(ecAttachment);
        payload.setExpenseCards(expenseCards);
        expenseRequest.setPayload(payload);

        HttpEntity<Object> object = restApiConnector.connectorPostHttp(expenseRequest, K2konnectEndpoint.URL_EXPENSE);


        if(object != null){

        }



       // ObjectMapper objectMapper = new ObjectMapper();
        //
       // UserInfo userInfo = null;

        //userInfo = objectMapper.readValue(object.getBody().toString(), UserInfo.class);
        //userInfo = objectMapper.convertValue(object.getBody(), UserInfo.class);

        return null;
    }
}
