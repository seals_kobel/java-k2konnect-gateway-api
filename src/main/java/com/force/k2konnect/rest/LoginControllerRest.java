package com.force.k2konnect.rest;

import com.force.k2konect.model.User;
import com.force.k2konect.model.UserInfo;
import com.force.k2konnect.http.HttpResponse;
import com.force.k2konnect.service.LoginService;
import com.force.k2konnect.util.HTTPStatusCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/login")
public class LoginControllerRest {

    @Autowired
    private LoginService loginService;

    @CrossOrigin
    @RequestMapping(value = "/consultant", method = RequestMethod.POST)
    public HttpResponse login(@RequestBody User user) {

        HttpResponse httpResponse = new HttpResponse();
        
        try {
            UserInfo userInfo = loginService.getLogin(user);
            boolean success = true;

            if (userInfo != null) {

                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_200.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_200.getDescription());
                httpResponse.setObject(userInfo);
            } else {

                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_204.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_204.getDescription());
                httpResponse.setObject(new String("User not found"));

            }

        } catch (Exception ex) {

            httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_500.getStatusCode());
            httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_500.getDescription());
            httpResponse.setObject(new String(ex.getMessage()));

        }

        return httpResponse;

    }


    @CrossOrigin
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public HttpResponse login_test() {

        HttpResponse httpResponse = new HttpResponse();

        try {
            UserInfo userInfo = loginService.getLogin(new User());

            boolean success = true;

            if (success) {

                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_200.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_200.getDescription());
                httpResponse.setObject(userInfo);
            } else {

                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_204.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_204.getDescription());
                httpResponse.setObject(new String("User not found"));

            }

        } catch (Exception ex) {

            httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_500.getStatusCode());
            httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_500.getDescription());
            httpResponse.setObject(new String(ex.getMessage()));

        }

        return httpResponse;

    }

}
