package com.force.k2konnect.rest;

import com.force.k2konect.model.RequestPhoto;
import com.force.k2konnect.http.HttpResponse;
import com.force.k2konnect.service.ExpenseService;
import com.force.k2konnect.util.HTTPStatusCodeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by eduardoabreu on 03/04/17.
 */
@RestController
@RequestMapping(value = "/expense")
public class ExpenseControllerRest {

    @Autowired
    private ExpenseService expenseService;

    @CrossOrigin
    @RequestMapping(value = "/upload/image", method = RequestMethod.POST)
    public HttpResponse listAllOccurrenceOpenByUser(@RequestBody RequestPhoto requestPhoto) {

        HttpResponse httpResponse = new HttpResponse();
        try {
            boolean isUpload = true;// occurrenceService.uploadPhoto(requestOcurrencePhoto);

            expenseService.sendExpense(requestPhoto);

            if (isUpload) {

                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_200.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_200.getDescription());
                httpResponse.setObject(new String("Upload realizado com sucesso"));
            } else {
                httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_204.getStatusCode());
                httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_204.getDescription());
                httpResponse.setObject(new String("Não foi possivel fazer upload foto"));
            }

        } catch (Exception e) {
            httpResponse.setResponseCode(HTTPStatusCodeEnum.STATUS_200.getStatusCode());
            httpResponse.setResponseDescription(HTTPStatusCodeEnum.STATUS_200.getDescription());
            httpResponse.setObject(new String(e.getMessage()));
        }

        return httpResponse;
    }
}
