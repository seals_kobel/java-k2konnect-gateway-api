package com.force.k2konnect.exception;

import java.text.MessageFormat;

/**
 * Created by eduardoabreu on 02/04/17.
 */
public class K2konnectException extends RuntimeException {

    private static final long serialVersionUID = 3422427928773141388L;

    protected Object[] params;

    public K2konnectException(final String mensagem) {
        super(mensagem);
    }

    public K2konnectException(final String mensagem, final Object[] params) {
        super(MessageFormat.format(mensagem, params));
    }

    public K2konnectException(final String mensagem, final Throwable cause) {
        super(mensagem, cause);
    }

    public K2konnectException(final String mensagem, final Object[] params, final Throwable cause) {
        super(MessageFormat.format(mensagem, params), cause);
    }

    protected K2konnectException() {
        super();
    }

    public K2konnectException(final Throwable cause) {
        super(cause);
    }

    public Object[] getParams() {
        return params;
    }

}