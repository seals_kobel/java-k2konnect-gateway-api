package com.force.k2konnect.http;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by eduardoabreu on 06/12/16.
 */
@Configuration
public class K2konnectEndpoint {

    public static String URL_BASE = "https://yourInstance.salesforce.com/services/data/";

    public static String URL_LOGIN = "https://test.salesforce.com/services/oauth2/token";

    public static String URL_EXPENSE = "https://k2filecompressor--dev7.cs12.my.salesforce.com/services/apexrest/expenses/doPost";

    public static String URL_PUSH_CREATE_PENDENCY =  "/pendency/create/push";

    public static String URL_PUSH_CREATE_OCCURRENCE =  "/occurrence/create/push";

    public static String URL_PUSH_CREATE_VISIT    = "/visit/push";

    public static String URL_PUSH_DELETE_VISIT    = "/visit/delete/push";

    public static String URL_PUSH_DELETE_ITINERARY    = "/itinerary/delete/push";


}
