package com.force.k2konnect.http;

/**
 * Created by eduardoabreu on 17/10/16.
 */

public class HttpResponse extends HeaderHttpResponse{

    private Object Object;

    public java.lang.Object getObject() {
        return Object;
    }

    public void setObject(java.lang.Object object) {
        Object = object;
    }
}
