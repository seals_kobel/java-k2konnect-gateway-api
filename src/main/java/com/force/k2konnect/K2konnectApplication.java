package com.force.k2konnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K2konnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(K2konnectApplication.class, args);
	}
}
