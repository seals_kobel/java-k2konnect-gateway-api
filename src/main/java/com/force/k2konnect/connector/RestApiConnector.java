package com.force.k2konnect.connector;

import com.force.k2konnect.exception.K2konnectException;
import org.springframework.http.HttpEntity;


public interface RestApiConnector {

	HttpEntity<Object> connectorPostHttp(Object objectDTO, final String uri)
			throws K2konnectException;

	HttpEntity<String> connectorGetHttp(Object objectDTO, final String uri)
			throws K2konnectException;
}
