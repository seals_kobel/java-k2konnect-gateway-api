package com.force.k2konnect.connector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.force.k2konnect.exception.K2konnectException;
import com.sun.deploy.net.HttpRequest;
import com.sun.deploy.net.HttpResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static com.sun.javafx.tools.resource.DeployResource.Type.data;


@Component
public class RestApiConnectorImpl implements RestApiConnector {

	@Override
	public HttpEntity<Object> connectorPostHttp(Object objectDTO, final String uri)
			throws K2konnectException {

		try {



			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			//headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.setContentType(MediaType.APPLICATION_JSON);

			headers.set("Authorization", "Bearer 00DV0000006tO1H!ASAAQLX4.eIN6xvLejsAvb5j77KSXMGTsKecPYpfwFpub275WHDk579rX9Kgj4Z3vrE1dyz.5YT08ewOciNqsWOkMtM_Y4ia");
			//String content = "username=tbalancin@k2partnering.com.dev7&client_secret=3696027746615538381&password=Teste1234&grant_type=password&client_id=3MVG9Oe7T3Ol0ea4t2O2mDN1fArM_xKAbcxHMIEz249W7J3lABO.Fm3mPt3WRfIEkdkzhrTbTyNmxTLSuCqdN";


			ObjectMapper mapper = new ObjectMapper();
			//Staff obj = new Staff();


			//mapper.writeValue(new File("c:\\file.json"), obj);

//Object to JSON in String
			String jsonInString = mapper.writeValueAsString(objectDTO);

			String params = objectDTO.toString();

			//System.out.println(content);
			System.out.println(jsonInString);

			HttpEntity<Object> entity = new HttpEntity<Object>(params, headers);
			HttpEntity<Object> result = restTemplate.exchange(uri, HttpMethod.POST, entity, Object.class);





			return result;
		} catch (Exception ex) {
			System.out.println("Error ---- " + ex.getMessage());
			return null;
		}

	}


	
	@Override
	public HttpEntity<String> connectorGetHttp(Object objectDTO, final String uri)
			throws K2konnectException {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set("Token", "nicbrain-supervisor-service");
		HttpEntity<Object> entity = new HttpEntity<Object>(objectDTO, headers);
		HttpEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
		
		return result;
	}
	
}
