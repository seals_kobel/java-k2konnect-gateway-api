package com.force.k2konect.model;

import java.util.List;

/**
 * Created by eduardoabreu on 03/04/17.
 */
public class Payload {

    private String Id;
    private String Status;
    private String PeriodId;
    private List<ExpenseCards> ExpenseCards;

    private String DealsheetId;
    private String BillingPeriodId;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPeriodId() {
        return PeriodId;
    }

    public void setPeriodId(String periodId) {
        PeriodId = periodId;
    }


    public String getDealsheetId() {
        return DealsheetId;
    }

    public void setDealsheetId(String dealsheetId) {
        DealsheetId = dealsheetId;
    }

    public String getBillingPeriodId() {
        return BillingPeriodId;
    }

    public void setBillingPeriodId(String billingPeriodId) {
        BillingPeriodId = billingPeriodId;
    }

    public List<com.force.k2konect.model.ExpenseCards> getExpenseCards() {
        return ExpenseCards;
    }

    public void setExpenseCards(List<com.force.k2konect.model.ExpenseCards> expenseCards) {
        ExpenseCards = expenseCards;
    }
}
