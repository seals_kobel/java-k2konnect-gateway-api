package com.force.k2konect.model;

/**
 * Created by eduardoabreu on 02/04/17.
 */
public class User {

    private String username;

    private String password;

    private String grant_type;

    private String client_id;

    private String client_secret;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    @Override
    public String toString() {
        return
                "username=" + username  +
                "&password=" + password +
                "&grant_type=" + grant_type +
                "&client_id=" + client_id +
                "&client_secret=" + client_secret ;
    }

}
