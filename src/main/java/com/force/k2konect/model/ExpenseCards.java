package com.force.k2konect.model;

/**
 * Created by eduardoabreu on 03/04/17.
 */
public class ExpenseCards {

    private int EcValue;
    private String EcType;
    private int EcTransactionValue;
    private int EcExchangeRate;
    private String EcDescription;
    private String EcDate;
    private String EcCurrency;
    private String EcComments;
    private String EcClient;
    private EcAttachment EcAttachment;


    public int getEcValue() {
        return EcValue;
    }

    public void setEcValue(int ecValue) {
        EcValue = ecValue;
    }

    public String getEcType() {
        return EcType;
    }

    public void setEcType(String ecType) {
        EcType = ecType;
    }

    public int getEcTransactionValue() {
        return EcTransactionValue;
    }

    public void setEcTransactionValue(int ecTransactionValue) {
        EcTransactionValue = ecTransactionValue;
    }

    public int getEcExchangeRate() {
        return EcExchangeRate;
    }

    public void setEcExchangeRate(int ecExchangeRate) {
        EcExchangeRate = ecExchangeRate;
    }

    public String getEcDescription() {
        return EcDescription;
    }

    public void setEcDescription(String ecDescription) {
        EcDescription = ecDescription;
    }

    public String getEcDate() {
        return EcDate;
    }

    public void setEcDate(String ecDate) {
        EcDate = ecDate;
    }

    public String getEcCurrency() {
        return EcCurrency;
    }

    public void setEcCurrency(String ecCurrency) {
        EcCurrency = ecCurrency;
    }

    public String getEcComments() {
        return EcComments;
    }

    public void setEcComments(String ecComments) {
        EcComments = ecComments;
    }

    public String getEcClient() {
        return EcClient;
    }

    public void setEcClient(String ecClient) {
        EcClient = ecClient;
    }

    public com.force.k2konect.model.EcAttachment getEcAttachment() {
        return EcAttachment;
    }

    public void setEcAttachment(com.force.k2konect.model.EcAttachment ecAttachment) {
        EcAttachment = ecAttachment;
    }

    @Override
    public String toString() {
        return "ExpenseCards{" +
                "EcValue=" + EcValue +
                ", EcType='" + EcType + '\'' +
                ", EcTransactionValue=" + EcTransactionValue +
                ", EcExchangeRate=" + EcExchangeRate +
                ", EcDescription='" + EcDescription + '\'' +
                ", EcDate='" + EcDate + '\'' +
                ", EcCurrency='" + EcCurrency + '\'' +
                ", EcComments='" + EcComments + '\'' +
                ", EcClient='" + EcClient + '\'' +
                ", EcAttachment=" + EcAttachment +
                '}';
    }
}
