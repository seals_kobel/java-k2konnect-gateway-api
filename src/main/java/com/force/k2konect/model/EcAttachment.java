package com.force.k2konect.model;

/**
 * Created by eduardoabreu on 03/04/17.
 */
public class EcAttachment {
    private String FileName;
    private String ContentType;
    private String Base64Data;

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String contentType) {
        ContentType = contentType;
    }

    public String getBase64Data() {
        return Base64Data;
    }

    public void setBase64Data(String base64Data) {
        Base64Data = base64Data;
    }
}
